@extends('layouts.app-auth')

@section('content')
<style>
    .table th, .table td {
        padding: 0.4rem!important;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">{{ $org_name }}</div>

                <div class="card-body">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
				        <thead>
				            <tr>
				                <th>Name</th>
                                <th>Shift Date</th>
				                <th>Start Time</th>
                                <th>Finish Time</th>
                                <th>Break Length <br/>(Mins)</th>
                                <th>Worked Hours</th>
                                <th>Shift Cost</th>
                                <th>Actions</th>
				            </tr>
				        </thead>
                        <tbody>
                            @foreach ($listShifts as $shift)
                            <tr>
                                <td>{{ $shift['name'] }}</td>
                                <td>{{ $shift['shift_date'] }}</td>
                                <td>{{ $shift['start_time'] }}</td>
                                <td>{{ $shift['finish_time'] }}</td>
                                <td>{{ $shift['break_length'] }}</td>
                                <td>{{ $shift['total_ws'] }}</td>
                                <td>$ {{ $shift['total_pay'] }}</td>
                                <td>
                                    <a class="btn btn-warning float-left" href="{{ route('shift.edit', $shift['id']) }}">
                                      Edit
                                    </a>
                                    <br/>
                                    <form method="post" class="float-left" action="{{ route('shift.destroy', $shift['id']) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Remove</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                            <form method="post" class="float-right" action="{{ route('shift.store') }}">
                                @csrf
                                <tr>
                                    <td>
                                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                                        {{ $user->name }}
                                    </td>
                                    <td>
                                        <input id="shift_date" type="date" name="shift_date" required autofocus>
                                    </td>
                                    <td>
                                        <input id="start_time" type="time" name="start_time" required autofocus>
                                    </td>
                                    <td>
                                        <input id="finish_time" type="time" name="finish_time" required autofocus>
                                    </td>
                                    <td>
                                        <input id="break_length" type="number" name="break_length" placeholder="0" required autofocus>
                                    </td>
                                    
                                    <td colspan="3">
                                    	<input type="hidden" name="o_id" value="{{ $o_id }}"/>
                                    	<button type="submit" class="btn btn-success">Create Shift</button>
                                    	
                                    </td>
                                </tr>
                            </form>

                            
                        </tbody>
				    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection
