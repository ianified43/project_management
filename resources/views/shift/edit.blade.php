@extends('layouts.app-auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Organisation</div>

                <div class="card-body">
                    
                            <form method="post" action="{{ route('shift.update', $shift['id']) }}">
                                @method('PUT')
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Name</label>

                                    <div class="col-md-6">
                                        {{ $emp_name }}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Shift Date</label>

                                    <div class="col-md-6">
                                        <input id="shift_date" type="date" value="{{ $shift['shift_date'] }}" name="shift_date" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Start Time</label>

                                    <div class="col-md-6">
                                        <input id="start_time" value="{{ $shift['start_time'] }}" type="time" name="start_time" required autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Finish Time</label>

                                    <div class="col-md-6">
                                        <input id="finish_time" value="{{ $shift['finish_time'] }}" type="time" name="finish_time" required autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Break Length (Mins)</label>

                                    <div class="col-md-6">
                                        <input id="break_length" value="{{ $shift['break_length'] }}" type="number" name="break_length" placeholder="0" required autofocus>
                                    </div>
                                </div>
                                

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-success">
                                            Save
                                        </button>

                                    </div>
                                </div>
                                
                            </form>

                            
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection
