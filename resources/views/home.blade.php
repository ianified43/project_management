@extends('layouts.app-auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    @if ($organisation == 0)
                        You aren't a member of any organisations. Join an existing one or create a new one.
                        <br/><br/><br/>
                        @if ($org_count != 0)
                        <a href="organisation" class="btn btn-success">Join an Organisations</a>
                        @endif
                        <a href="organisation/create" class="btn btn-success">Create an Organisation</a>
                    @else
                        <h2>Organizations</h2>
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Hourly Rate</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($organisations_assigned as $organisation_assigned)
                                <tr>
                                    <td>{{ $organisation_assigned->name }}</td>
                                    <td>$ {{ $organisation_assigned->hourly_rate }}</td>
                                    <td>
                                        &nbsp;&nbsp;
                                        <form method="post" class="float-right" action="organisation/cancel_org">
                                            @csrf
                                            <input type="hidden" name="o_id" value="{{ $organisation_assigned->id }}"/>
                                            <button type="submit" class="btn btn-danger">Cancel</button>
                                        </form>
                                        &nbsp;&nbsp;
                                        <a class="btn btn-warning float-right" href="{{ route('organisation.edit',$organisation_assigned->id) }}">
                                          Edit
                                        </a>
                                        &nbsp;&nbsp;
                                        <a class="btn btn-success float-right" href="{{ route('shift.show',$organisation_assigned->id) }}">
                                          View Shifts
                                        </a>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
