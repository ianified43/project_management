@extends('layouts.app-auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Organisation</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('organisation.update', $organisation->id) }}">
                        @method('PATCH')
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" name="name" value="{{ $organisation->name }}" placeholder="Name" required autofocus>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Hourly Rate</label>

                            <div class="col-md-6">
                                $ <input id="hourly_rate" type="number" value="{{ $organisation->hourly_rate }}" name="hourly_rate" placeholder="0.00" required autofocus> per hour

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    Save
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
