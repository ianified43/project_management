@extends('layouts.app-auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">Organisations <a class="btn btn-success float-right" href="organisation/create">
                  New Organisation
                </a></div>

                <div class="card-body">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
				        <thead>
				            <tr>
				                <th>Name</th>
                                <th>Hourly Rate</th>
				                <th>Actions</th>
				            </tr>
				        </thead>
                        <tbody>
                            @foreach ($organisations as $organisation)
                            <tr>
                                <td>{{ $organisation->name }}</td>
                                <td>$ {{ $organisation->hourly_rate }}</td>
                                <td>
                                	&nbsp;&nbsp;
                                	<form method="post" class="float-right" action="organisation/join_org">
                                		@csrf
                                		<input type="hidden" name="o_id" value="{{ $organisation->id }}"/>
                                		<button type="submit" class="btn btn-success">Join</button>
                                	</form>
					                &nbsp;&nbsp;
					                <a class="btn btn-warning float-right" href="{{ route('organisation.edit',$organisation->id) }}">
					                  Edit
					                </a>
					                &nbsp;&nbsp;
                                </td>
                            </tr>

                            @endforeach
                        </tbody>
				    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

@endsection
