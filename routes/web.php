<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->middleware('auth');

//Admin Routes
Auth::routes();


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group([
    'middleware' => ['auth']
], function () {

    Route::get('/home', 'HomeController@index');
    Route::post('organisation/join_org', 'OrganisationController@join_org');
    Route::post('organisation/cancel_org', 'OrganisationController@cancel_org');
    Route::resource('organisation', 'OrganisationController');
    Route::resource('shift', 'ShiftController');
    

});
