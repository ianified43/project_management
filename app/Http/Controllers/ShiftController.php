<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Organisation;
use App\Model\OrganisationUsers;
use App\Model\User;
use App\Model\Shift;
use Auth;

class ShiftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Shift $shift)
    {
        $request->validate([
            'user_id'=>'required',
            'shift_date'=> 'required',
            'start_time'=> 'required',
            'finish_time'=> 'required',
            'break_length'=> 'required',
            'o_id' => 'required',
        ]);

        $shift->user_id = $request->get('user_id');
        $shift->organisation_id = $request->get('o_id');
        $shift->shift_date = $request->get('shift_date');
        $shift->start_time = $request->get('start_time');
        $shift->finish_time = $request->get('finish_time');
        $shift->break_length = $request->get('break_length');

        $shift->save();

        return redirect()->back();

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auth_id = Auth::user()->id;
        $user = User::findOrFail($auth_id);

        $organisation = Organisation::findOrFail($id);

        $users = $organisation->users;
        $o_id = $id;
        $org_name = $organisation->name;

        $org_hourly_rate = $organisation->hourly_rate;


        $shifts = Shift::ofOrganisationId($id)->get();

        $listShifts = [];
        $x = 0;
        foreach ($shifts as $shift) {

            $employee = User::findOrFail($shift->user_id);

            $listShifts[$x]['name'] = $employee->name;

            $listShifts[$x]['id'] = $shift->id;



            $listShifts[$x]['shift_date'] = date('M d, Y', strtotime($shift->shift_date));

            $listShifts[$x]['start_time'] = date('h:i a', strtotime($shift->start_time));
            $listShifts[$x]['finish_time'] = date('h:i a', strtotime($shift->finish_time));

            $totalWS = 0;

            $rate = 0;

            

            if(date('N', strtotime($shift->shift_date)) == 7){
                if(strtotime($shift->start_time) > strtotime($shift->finish_time)){

                   
                    $time1 = strtotime($shift->start_time);
                    $time2 = strtotime('24:00:00');
                    $difference1 = round(abs($time2 - $time1) / 3600,2);
                    $doubled = $difference1 * $org_hourly_rate * 2;

                    $time3 = strtotime($shift->finish_time);
                    $time4 = strtotime('00:00:00');


                    $difference2 = (round(abs($time3 - $time4) / 60,2) - $shift->break_length)/ 60;
                    $basic = $difference2 * $org_hourly_rate;

                    if($difference2 < 0){
                        $rate = ($difference1 + $difference2) * $org_hourly_rate * 2;
                    }else{
                        $rate = $doubled + $basic;
                    }
                    

                    $listShifts[$x]['total_ws'] = number_format($difference1 + $difference2, 2);

                }else{
                    $totalWS = strtotime($shift->finish_time) - strtotime($shift->start_time);

                    $rate = number_format((((($totalWS/60) - $shift->break_length)/60) * $org_hourly_rate)*2, 2);
                    $listShifts[$x]['total_ws'] = number_format((($totalWS/60) - $shift->break_length)/60, 2);
                }
            }else{
                if(strtotime($shift->start_time) > strtotime($shift->finish_time)){

                    $totalWS = strtotime($shift->finish_time . ' +1 day') - strtotime($shift->start_time);

                }else{
                    $totalWS = strtotime($shift->finish_time) - strtotime($shift->start_time);
                }

                $rate = number_format(((($totalWS/60) - $shift->break_length)/60) * $org_hourly_rate, 2);

                $listShifts[$x]['total_ws'] = number_format((($totalWS/60) - $shift->break_length)/60, 2);
            }



            

            $listShifts[$x]['break_length'] = $shift->break_length;
            $listShifts[$x]['total_pay'] = number_format($rate,2);

            $x++;
        }



        return view('shift/list', compact('users', 'user', 'o_id', 'org_name', 'listShifts')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shift = Shift::findOrFail($id);

        $auth_id = Auth::user()->id;
        $user = User::findOrFail($auth_id);

        $employee = User::findOrFail($shift->user_id);

        $emp_name = $employee->name;

        return view('shift/edit', compact('user', 'shift', 'emp_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'shift_date'=> 'required',
            'start_time'=> 'required',
            'finish_time'=> 'required',
            'break_length'=> 'required'
        ]);

        $shift = Shift::find($id);

        $oid = $shift->organisation_id;

        $shift->shift_date = $request->get('shift_date');
        $shift->start_time = $request->get('start_time');
        $shift->finish_time = $request->get('finish_time');
        $shift->break_length = $request->get('break_length');

        $shift->save();

        return redirect()->route('shift.show', $oid);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $shift = Shift::find($id);
        $shift->delete();


        return redirect()->back();
    }
}
