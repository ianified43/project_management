<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Organisation;
use App\Model\OrganisationUsers;
use App\Model\User;
use Auth;

class OrganisationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $organisations_all = Organisation::all();

        $allOfUsers = $user->organisations;

        $organisations = $organisations_all->diff($allOfUsers);

        return view('organisation/home', compact('organisations', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        return view('organisation/create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Organisation $organisation,Request $request)
    {
        $request->validate([
            'name'=>'required',
            'hourly_rate'=> 'required'
        ]);
        $organisation->name = $request->get('name');
        $organisation->hourly_rate = $request->get('hourly_rate');

        $organisation->save();

        return redirect()->route('organisation.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auth_id = Auth::user()->id;
        $user = User::findOrFail($auth_id);
        $organisation = Organisation::findOrFail($id);
        return view('organisation/edit', compact('organisation', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'hourly_rate'=> 'required'
        ]);

        $organisation = Organisation::find($id);
        $organisation->name = $request->get('name');
        $organisation->hourly_rate = $request->get('hourly_rate');

        $organisation->save();

        return redirect()->route('organisation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function join_org(OrganisationUsers $organisationUsers, Request $request)
    {
        
        $organisationUsers->organisation_id = $request->o_id;
        $organisationUsers->user_id = Auth::user()->id;
        $organisationUsers->save();

        return redirect()->route('organisation.index');
    }

    public function cancel_org(OrganisationUsers $organisationUsers, Request $request)
    {
        
        $whereArray = array('user_id' => Auth::user()->id,'organisation_id' => $request->o_id);
        $res = OrganisationUsers::where($whereArray)->delete();

        return redirect()->back();
    }
}
