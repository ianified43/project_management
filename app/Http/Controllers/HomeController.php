<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Organisation;
use Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $organisation = $user->organisations()->count();

        $org_count = Organisation::all()->count();

        $organisations_assigned = $user->organisations;

        return view('home', compact('user', 'organisation', 'organisations_assigned', 'org_count'));
    }
}
