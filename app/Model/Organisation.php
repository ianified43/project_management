<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    public function users(){
    	return $this->belongsToMany(User::class, 'organisation_users');
	}

}
