<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function organisations()
    {
        return $this->belongsTo(Organisation::class);
    }

    public function scopeOfOrganisationId($query, $id)
    {
        return $query->whereOrganisationId($id);
    }


}
