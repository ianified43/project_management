<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'start_date', 'end_date', 'extend_date', 'title', 'project_id', 'award_id', 'implementing_partner', 'fund_source',
    ];

    protected $casts = [
        'start_date' => 'date', 'end_date' => 'date', 'extend_date' => 'date',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
